import os.path

from vts.harnesses.host_controller.build import pab_client

ALKALI_ACCOUNT_ID = 100621237


class Client(pab_client.PartnerAndroidBuildClient):
    CLIENT_SECRETS = os.path.join(
        os.path.dirname(__file__), 'client_secrets.json')
    CLIENT_STORAGE = os.path.join(os.path.dirname(__file__), 'credentials')

    @property
    def tmp_dirpath(self):
        return None


def main():
    client = Client()
    client.Authenticate()
    client.GetArtifact(
        account_id=ALKALI_ACCOUNT_ID,
        branch='aosp-master-ndk',
        target='linux',
        artifact_name='android-ndk-{build_id}-linux-x86_64.tar.bz2',
    )


if __name__ == '__main__':
    main()
